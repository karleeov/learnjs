const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const map1 = arr.map((arr) => "arr" + arr);

console.log(map1);

// same for each

const doubleNumbers = [];

arr.forEach((arr) => {
  doubleNumbers.push(arr * 2);
});

console.log(doubleNumbers);

const companies = [
  { name: "company one", category: "retail", start: 1988, end: 2000 },
  { name: "company two", category: "shop", start: 1988, end: 2000 },
  { name: "company three", category: "retail", start: 1988, end: 2000 },
  { name: "company four", category: "retail", start: 1988, end: 2030 },
];

const companyName = companies.map((company) => company.name);

console.log(companyName);

// create arra and category

const companyInfo = companies.map((company) => {
  return {
    name: company.name,
    category: company.category,
  };
});

console.log(companyInfo);

// create array for company years

const companyYears = companies.map((company) => {
  return {
    name: company.name,
    length: company.end - company.start + "     years",
  };
});

console.log(companyYears);

// chain map method

// const testMap = arr.map((arr) => Math.sqrt(arr)).map((sqrt) => sqrt * 2);

// chaining different method

const evendouble = arr
  .filter((arr) => arr % 2 === 0)
  .map((arr) => arr * 100)
  .map((arr) => arr + "crazy double")
  .map((arr) => arr + 1000 + "lets keep going ");

console.log(evendouble);
