for (let i = 1; i < 100; i++) {
  console.log(i);
  if (i % 5 === 0 && i % 3 === 0) {
    console.log("fizzbuzz");
  } else if (i % 5 === 0) {
    console.log("buzz");
  } else if (i % 3 === 0) {
    console.log("three");
  } else {
    console.log(i);
  }
}

let j = 1;

while (j <= 100) {
  if (j % 5 === 0 && j % 3 === 0) {
    console.log("fizzbuzz");
  } else if (j % 5 === 0) {
    console.log("buzz");
  } else if (j % 3 === 0) {
    console.log("three");
  } else {
    console.log("while", j);
  }
  j++;
}
