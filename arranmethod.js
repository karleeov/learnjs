const people = [
  {
    firstName: "John",
    lastName: "Doe",
    email: "john@gmail.com",
    phone: "111-111-1111",
    age: 30,
  },
  {
    firstName: "Jane",
    lastName: "Poe",
    email: "jane@gmail.com",
    phone: "222-222-2222",
    age: 25,
  },
  {
    firstName: "Bob",
    lastName: "Foe",
    email: "bob@gmail.com",
    phone: "333-333-3333",
    age: 45,
  },
  {
    firstName: "Sara",
    lastName: "Soe",
    email: "Sara@gmail.com",
    phone: "444-444-4444",
    age: 19,
  },
  {
    firstName: "Jose",
    lastName: "Koe",
    email: "jose@gmail.com",
    phone: "555-555-5555",
    age: 23,
  },
];

const peopleInfo = people
  .filter((person) => person.age <= 25)
  .map((person) => ({
    name: person.name,
    email: person.email,
  }));

console.log(peopleInfo);

const numbers = [2, -1, 3, 40, -12];

const sum = numbers
  .filter((numbers) => numbers > 0)
  .reduce((acc, cur) => acc + cur, 0);

console.log(sum);

const words = [
  "coder",
  "programmer",
  "developer",
  "engineer",
  "software engineer",
];

const cWords = words.filter((word) => {
  return word[0].toUpperCase() + word.slice(2, word.length);
});

console.log(cWords);
