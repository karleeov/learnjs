const age = 30;

if (age >= 19) {
  console.log("you can vote");
} else {
  console.log("you cannot vote");
}

// tenary opreatior
age >= 18 ? console.log("you can vote!") : console.log("you cannot");
const canVote = age >= 18 ? true : false;
const canVote2 = age >= 12 ? true : false;

console.log(canVote);
console.log(canVote2);

const auth = true;

// let redirect;

// if (auth) {
//   alert("you are login bitch");
//   redirect = "/home";
// } else {
//   alert("failed");
//   redirect = "/login";
// }

// console.log(redirect);

// const login = auth
//   ? (alert("haha"), 'redirect = "home"')
//   : (redirect = "/login");

// console.log(login);

auth ? console.log("welcome to the dashboard") : null;

auth && console.log("welcome to here");
