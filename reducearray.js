const array1 = [1, 2, 3, 24, 235, 325, 235, 234, 234];

const sum = array1.reduce(function (accmulator, currentValue) {
  return accmulator + currentValue;
}, 0);

const sum1 = array1.reduce((acc, cur) => acc + cur, 1000);

// for loop

const sumsum = () => {
  let acc = 1000;
  for (const cur of array1) {
    acc = acc + cur;
  }
  return acc;
};
console.log(sumsum());

// shoppping cart

const cart = [
  { id: 1, name: "clothes", price: 1000 },
  { id: 2, name: "shirts", price: 1000 },
  { id: 3, name: "pants", price: 1000 },
];

const total = cart.reduce((acc, cur) => acc + cur.price, 0);

console.log(total);
