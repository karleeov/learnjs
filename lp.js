console.log(10 > 20 && 30 > 15 && 40 > 30);
console.log(10 > 20 || 30 < 15);

let a;

a = 10 && 20 && "" && 10;
const posts = ["post post"];

posts.length > 0 && console.log(posts[0]);
console.log(a);

let b;
// b = 10 || 20;
b = 0 || 12 || "" || undefined;
c = null ?? undefined;

console.log(b);
console.log(c);
