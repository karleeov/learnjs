for (let i = 0; i < 10; i++) {
  console.log("hello karl", i);
}

const numbers = [1, 2, 3, 4, 5, 6, 4, 64];
const word = ["hong kong", "thailand "];

// const result = word.filter((word) => word.length > 10);

// const evenNumbers = numbers.filter(function (number) {
//   return number % 2 === 0;
// });

// const evenNumbers = numbers.filter((number) => number % 2 === 0);

//foreach method
const evenNumbers = [];
numbers.forEach((number) => {
  if (number % 2 === 0) evenNumbers.push(number);
});

const companies = [
  { name: "company one", category: "retail", start: 1988, end: 2000 },
  { name: "company two", category: "shop", start: 1988, end: 2000 },
  { name: "company three", category: "retail", start: 1988, end: 2000 },
  { name: "company four", category: "retail", start: 1988, end: 2030 },
];

const retailCompanies = [];
companies.forEach((companies) => {
  console.log(companies);
  if (companies.category === "retail") {
    retailCompanies.push(companies);
  }
});

const retail = companies.filter((companies) => companies.category === "retail");

// console.log(retail);
// console.log(retailCompanies);

const earlyCompanies = companies.filter(
  (company) => company.start >= 1980 && company.end <= 2000
);

console.log(earlyCompanies);

// get Companies that lasted 10 years

const LastedTenyears = companies.filter(
  (company) => company.end - company.start >= 20
);

console.log(LastedTenyears);
