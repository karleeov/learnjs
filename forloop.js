const items = ["book", "cart", "chair", "maureen", "lai"];

const users = [{ name: "king" }, { name: "peter" }];

for (let i = 0; i < items.length; i++) {
  console.log(items[i]);
}

for (const item of items) {
  console.log(item);
}

for (const user of users) {
  console.log(user.name);
}

//loop over

const str = "helllooo";

for (const letter of str) {
  console.log(letter);
}

//loop over map

const map = new Map();
map.set("name", "john");
map.set("AGE", 20);

for (const [key, value] of map) {
  console.log(key, value);
}

//for in loop
const colorObj = {
  color: "red",
  color2: "blue",
  color3: "orange",
  color4: "pink",
};

for (const key in colorObj) {
  console.log(key, colorObj[key]);
}

const colorArr = ["red", "green", "yello"];

for (const key in colorArr) {
  console.log(colorArr[key]);
}
