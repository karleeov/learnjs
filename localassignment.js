let a = false;
if (!a) {
  a = 20;
}

a = a || 10;

a ||= 10;

// console.log(a);

// if (b) {
//   b = 20;
// }

// b &&= 30;

// console.log(b);

let c = null;
if (c === null || c === undefined) {
  c = 230;
}

c = c ??= 20;
console.log(c);
